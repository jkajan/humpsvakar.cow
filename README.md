# humpsvakar.cow

Om man är en hängiven användare av `cowsay`, men föredrar framom nötkreatur istället partåiga hovdjur av "humpsig" karaktär finns i detta repositorium två filer för att tillfredsställa detta begär.

## iines.cow

```
$ cowsay ./iines.cow muuu

 ______ 
< muuu >
 ------ 
    \   ^__^  __    __
     \  (oo)\///\__/\\\
        (__)//////////\\
            \\\\\\\\\\\\
            /  /   /  /
           _\ _\  _\ _\

```

## älgen.cow

```
$ cowsay ./älgen.cow -d huää 

 ________ 
< huää >
 -------- 
    \   \__/  __    __
     \  (xx)\///\__/\\\
        (##)//////////\\
         U  \\\\\\\\\\\\
            /  /   /   \
          _/  _\  _\   _\
```
